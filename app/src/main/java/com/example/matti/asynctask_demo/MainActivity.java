package com.example.matti.asynctask_demo;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity {

    private Button button;
    private TextView finalResult;
    private ImageView downloadedImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button) findViewById(R.id.btn_run);
        finalResult = (TextView) findViewById(R.id.tv_result);
        downloadedImage = (ImageView) findViewById(R.id.imageView);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AsyncTaskRunner runner = new AsyncTaskRunner();
                runner.execute();

            }
        });
    }

    // The types specified here are the input data type, the progress type, and the result type
    private class AsyncTaskRunner extends AsyncTask<String, String, Bitmap> {

        ProgressDialog progressDialog;


        @Override
        protected void onPreExecute() {

            // Runs on the UI thread before doInBackground
            // Good for toggling visibility of a progress indicator

            progressDialog = ProgressDialog.show(MainActivity.this,
                    "ProgressDialog",
                    "Downloading image ");

        }


        @Override
        protected Bitmap doInBackground(String... strings) {

            // Some long-running task like downloading an image.

            publishProgress("Downloading image..."); // Calls onProgressUpdate()

            String imageURL = "http://tiger.tas.edu.tw/ls/kg/webquests/animal%20research%20webquest/images/Koala.jpg";
            Bitmap bitmap = null;

            InputStream in = null;
            try {
                in = new java.net.URL(imageURL).openStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            bitmap = BitmapFactory.decodeStream(in);

            return bitmap;

        }


        @Override
        protected void onPostExecute(Bitmap bitmap) {

            // This method is executed in the UIThread
            // with access to the result of the long running task

            progressDialog.dismiss(); // This will hide the progress indicator

            downloadedImage.setImageBitmap(bitmap);
            finalResult.setText("Image downloaded succesfully!");

        }



        @Override
        protected void onProgressUpdate(String... text) {

            // Executes whenever publishProgress is called from doInBackground
            // Used to update the progress indicator

            finalResult.setText(text[0]);

        }
    }
}